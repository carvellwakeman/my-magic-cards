#!/bin/bash
# Update my-magic-cards config
/home/rfidmusic/my-magic-cards/setup/update.sh
# Mopidy consume on
( sleep 120; python3 /home/rfidmusic/my-magic-cards/app/config/mopidy.py consume ) &
( sleep 121; python3 /home/rfidmusic/my-magic-cards/app/config/mopidy.py playnow file:/home/rfidmusic/my-magic-cards/mopidy/music/startup.mp3 ) &
