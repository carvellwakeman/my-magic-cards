#!/bin/bash

# Run in the directory of this script
cd $(dirname $0)

# Raspberry Pi setup
echo Set GPU memory to minimum
sudo raspi-config nonint do_memory_split 16

echo Create rfidmusic user
# sudo useradd -m tempuser -s /bin/bash
# sudo passwd tempuser #enter pwd
# sudo usermod -a -G sudo tempuser
# logout, login as tempuser
# cd /etc #backup files
# sudo tar -cvf authfiles.tar passwd group shadow gshadow sudoers subuid subgid lightdm/lightdm.conf systemd/system/autologin@.service sudoers.d/010_pi-nopasswd systemd/system/getty@tty1.service.d/autologin.conf polkit-1/localauthority.conf.d/60-desktop-policy.conf
# sudo sed -i.$(date +'%y%m%d_%H%M%S') 's/\bpi\b/rfidmusic/g' passwd group shadow gshadow sudoers subuid subgid systemd/system/autologin@.service sudoers.d/010_pi-nopasswd systemd/system/getty@tty1.service.d/autologin.conf polkit-1/localauthority.conf.d/60-desktop-policy.conf
# sudo sed -i.$(date +'%y%m%d_%H%M%S') 's/user=pi/user=rfidmusic/' lightdm/lightdm.conf
# sudo mv /home/pi /home/rfidmusic
# sudo ln -s /home/rfidmusic /home/pi
# sudo [ -f /var/spool/cron/crontabs/pi ] && sudo mv -v /var/spool/cron/crontabs/pi /var/spool/cron/crontabs/rfidmusic
# sudo passwd rfidmusic #enter pwd
# logout, login as rfidmusic
# sudo userdel tempuser
# sudo pkill -u tempuser # if needed for above
sudo usermod -a -G input rfidmusic

# Python
sudo apt-get install python3-pip -y

# Get docker
# echo Install Docker
# sudo apt-get install docker-ce=18.06.2~ce~3-0~raspbian -y
# sudo usermod -G docker rfidmusic
# sudo pip3 -v install docker-compose

# Setup docker compose systemd
# echo Soft-link rfidmusic systemd service
# sudo rm /etc/systemd/system/rfidmusic.service
# sudo ln -s $(realpath ./rfidmusic.service) /etc/systemd/system/rfidmusic.service

# echo Soft-link docker-compose file and magic-cards config
# sudo rm /etc/docker/compose/rfidmusic
# sudo mkdir -p /etc/docker/compose/
# sudo ln -s $(realpath ../app) /etc/docker/compose/rfidmusic


# TODO: Magic-cards manual setup method
# sudo apt-get install git
# wget -O - https://raw.githubusercontent.com/sdesalas/node-pi-zero/master/install-node-v9.11.2.sh | bash
# export PATH=$PATH:/opt/nodejs/bin
# npm install yarn -g
# cd ~
# git clone git@github.com:maddox/magic-cards.git
# cd magic-cards
# script/setup # This never completes on a pi zero. Takes a couple minutes on a pi 4. Copy over scp.
# echo Link config to Magic-cards
# rm -rf /home/rfidmusic/magic-cards/config
# sudo ln -s $(realpath ../app/config) /home/rfidmusic/magic-cards/config
# echo Setup systemd for magic-cards services
# script/install

# Get mopidy
echo Get Mopidy and extensions
wget -q -O - https://apt.mopidy.com/mopidy.gpg | sudo apt-key add -
sudo wget -q -O /etc/apt/sources.list.d/mopidy.list https://apt.mopidy.com/stretch.list
sudo apt-get update -y
sudo apt-get install mopidy -y
# sudo apt-get install mopidy-mpd # Not found
sudo python3 -m pip install Mopidy-Spotify
sudo python3 -m pip install Mopidy-Local
sudo python3 -m pip install Mopidy-Mpd

# Mopidy systemd service (overrides /lib/systemd/system/mopidy.service)
echo Soft-link mopidy systemd service
sudo rm /etc/systemd/system/mopidy.service
sudo ln -s $(realpath ./mopidy.service) /etc/systemd/system/mopidy.service

# Mopidy config files
echo Setup Mopidy
sudo ln -s $(realpath ../mopidy/mopidy.conf) /etc/mopidy/mopidy.conf
sudo ln -s $(realpath ../mopidy/secrets.conf) /etc/mopidy/secrets.conf
# MANUAL: Copy or create secrets.conf in this folder
sudo systemctl daemon-reload
sudo systemctl enable mopidy
sudo systemctl start mopidy

# Allow audio output through HAT
echo The following script will setup audio output. Say Yes to everything and then reboot.
curl -sS https://raw.githubusercontent.com/adafruit/Raspberry-Pi-Installer-Scripts/master/i2samp.sh | bash

# Setup Pi to run magic-cards and mopidy on startup
echo Enable and start systemd service
sudo systemctl enable rfidmusic
sudo systemctl start rfidmusic
# sudo systemctl status rfidmusic.service

# Setup IR receiver
# 0x45 # 0x46 # 0x47 #
# 0x44 # 0x40 # 0x43 #
# 0x07 # 0x15 # 0x09 #
# 0x16 # 0x19 # 0x0d #
# 0x0c # 0x18 # 0x5e #
# 0x08 # 0x1c # 0x5a #
# 0x42 # 0x52 # 0x4a #

echo Update Driver file with IR gpio pin
sudo echo "dtoverlay=gpio-ir,gpio_pin=23" | sudo tee -a /boot/config.txt
echo Install IR keytable
sudo apt install ir-keytable
#sudo ir-keytable -c -p all -t # Read keycodes from remote and clear current keytable

#sudo ir-keytable # get driver info (after reboot)
#Found /sys/class/rc/rc0/ (/dev/input/event0) with:
#        Name: gpio_ir_recv
#        Driver: gpio_ir_recv, table: rc-rc6-mce
#        LIRC device: /dev/lirc0
#        Attached BPF protocols:
#        Supported kernel protocols: other lirc rc-5 rc-5-sz jvc sony nec sanyo mce_kbd rc-6 sharp xmp imon
#        Enabled kernel protocols: lirc rc-5 rc-5-sz jvc sony nec sanyo mce_kbd rc-6 sharp xmp imon
#        bus: 25, vendor/product: 0001:0001, version: 0x0100
#        Repeat delay = 500 ms, repeat period = 125 ms

echo Copy keymap file
sudo ln -s $(realpath ./keymap/rc6_rfidmusic) /etc/rc_keymaps/rc6_rfidmusic
echo Clear old keytable
sudo ir-keytable -c
echo Write keymap to driver
sudo ir-keytable -p NEC,RC6 -w /etc/rc_keymaps/rc6_rfidmusic
echo Write to rc rc_maps config
sudo echo "gpio_ir_recv rc-rc6-mce rc6_rfidmusic" | sudo tee -a /etc/rc_maps.cfg
echo Symbolic link keymap
sudo ln -s /etc/rc_keymaps/rc6_rfidmusic /lib/udev/rc_keymaps/rc6_rfidmusic

echo Reload keymaps on boot with cron job
sudo crontab ./crontab.txt
#sudo ir-keytable -c -w /etc/rc_keymaps/rc6_rfidmusic

# sudo echo "ACTION==\"add\", SUBSYSTEM==\"rc\", RUN+=\"/bin/ls -l /sys/class/rc &> /tmp/ls.txt\"" | sudo tee -a /lib/udev/rules.d/60-ir-keytable.rules


# Trigger happy to listen for input events
echo Install Triggerhappy
sudo apt-get install triggerhappy -y
echo Link scripts into /opt/scripts
sudo ln -s $(realpath ../scripts) /opt/scripts
echo Link Triggerhappy config
sudo ln -s $(realpath ./triggerhappy.conf) /etc/triggerhappy/triggers.d/triggerhappy.conf
sudo systemctl restart triggerhappy

# Manual
echo Run manually: sudo visudo
echo "# ADDED"
echo "nobody ALL =NOPASSWD: /sbin/shutdown*"