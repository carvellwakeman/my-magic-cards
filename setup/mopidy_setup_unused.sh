#!/bin/bash

# Run in the directory of this script
cd $(dirname $0)

# remove old config, copy new one
sudo rm -f /etc/mopidy/mopidy.conf
sudo mkdir -p /etc/mopidy/
sudo cp ../mopidy/mopidy.conf /etc/mopidy/

echo Set Mopidy-spotify credentials
echo "[spotify]" | sudo tee -a /etc/mopidy/mopidy.conf > /dev/null
# username = 
echo Enter spotify username:
read spotify_username
echo "username = "$spotify_username | sudo tee -a /etc/mopidy/mopidy.conf > /dev/null
# password = 
echo Enter spotify password:
read spotify_password
echo "password = "$spotify_password | sudo tee -a /etc/mopidy/mopidy.conf > /dev/null
# client_id = 
echo Enter spotify client_id:
read spotify_client_id
echo "client_id = "$spotify_client_id | sudo tee -a /etc/mopidy/mopidy.conf > /dev/null
# client_secret = 
echo Enter spotify client_secret:
read spotify_client_secret
echo "client_secret = "$spotify_client_secret | sudo tee -a /etc/mopidy/mopidy.conf > /dev/null
echo "Success"