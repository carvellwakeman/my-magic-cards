import subprocess, json, sys, time, socket, fcntl, struct

# Get hostname IP by interface
def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24])


# HOSTNAME = get_ip_address('wlan0')
HOSTNAME = '127.0.0.1'
URL = 'http://' + HOSTNAME + ':6680/mopidy/rpc'
HEADER = {'Content-Type': 'application/json'}
BODY_PLAY = {'jsonrpc': '2.0', 'id': 1, 'method': 'core.playback.play'}
BODY_PAUSE = {'jsonrpc': '2.0', 'id': 1, 'method': 'core.playback.pause'}
BODY_NEXT = {'jsonrpc': '2.0', 'id': 1, 'method': 'core.playback.next'}
BODY_PREV = {'jsonrpc': '2.0', 'id': 1, 'method': 'core.playback.previous'}
BODY_STATE = {'jsonrpc': '2.0', 'id': 1, 'method': 'core.playback.get_state'}
BODY_PLAY_NOW = {'jsonrpc': '2.0', 'id': 1, 'method': 'core.playback.play'}
BODY_TRACK_ADD = {'jsonrpc': '2.0', 'id': 1, 'method': 'core.tracklist.add'}
BODY_TRACK_CLEAR = {'jsonrpc': '2.0', 'id': 1, 'method': 'core.tracklist.clear'}
BODY_TRACK_SHUFFLE = {'jsonrpc': '2.0', 'id': 1, 'method': 'core.tracklist.shuffle'}
BODY_TRACK_CONSUME = {'jsonrpc': '2.0', 'id': 1, 'method': 'core.tracklist.set_consume'}
BODY_TRACK_LIST_LENGTH = {'jsonrpc': '2.0', 'id': 1, 'method': 'core.tracklist.get_length'}
# MPC
MPC_PLAY = ['mpc', 'play', '1']
MPC_PAUSE = ['mpc', 'pause']
MPC_TOGGLE = ['mpc', 'toggle']
MPC_NEXT = ['mpc', 'next']
MPC_PREV = ['mpc', 'prev']
MPC_STATE = ['mpc']
MPC_TRACK_ADD = ['mpc', 'add']
MPC_TRACK_CLEAR = ['mpc', 'clear']
MPC_TRACK_SHUFFLE = ['mpc', 'shuffle']
MPC_TRACK_CONSUME = ['mpc', 'del', '1']
MPC_VOLUME_SET = ['mpc', 'volume']
MPC_CONSUME_ON = ['mpc', 'consume', '1']


# Post request
def request_post(URL, data, headers):
    request = urllib2.Request(URL, json.dumps(data), headers)
    response = urllib2.urlopen(request)
    time.sleep(.300)
    return json.loads(response.read())

def mpc_run(command, params = 0):
    arr = command
    if params:
        arr += params
    result = subprocess.run(arr, capture_output=True)
    if (result.stdout):
        return (result.returncode,result.stdout.decode('utf-8'))
    else:
        return (result.returncode, '')

# Mopidy functions
def play():
    return mpc_run(MPC_PLAY)
    # return request_post(URL, data=BODY_PLAY, headers=HEADER)

def pause():
    return mpc_run(MPC_PAUSE)
    # return request_post(URL, data=BODY_PAUSE, headers=HEADER)

def play_now(track):
    if (track):
        track_clear()
        track_add(track)
    return play()

def play_now_if_empty(track):
    if (track_list_length() == 0):
        return play_now(track)

def toggle_play_pause():
    return mpc_run(MPC_TOGGLE)

def track_add(track):
    return mpc_run(MPC_TRACK_ADD, [track])
    # body = BODY_TRACK_ADD
    # body['params'] = {'uris': [track]}
    # return request_post(URL, data=body, headers=HEADER)

def track_clear():
    return mpc_run(MPC_TRACK_CLEAR)
    # return request_post(URL, data=BODY_TRACK_CLEAR, headers=HEADER)

def track_next():
    return mpc_run(MPC_NEXT)
    # return request_post(URL, data=BODY_NEXT, headers=HEADER)

def track_prev():
    return mpc_run(MPC_PREV)
    # return request_post(URL, data=BODY_PREV, headers=HEADER)

def track_list_length():
    state = mpc_run(MPC_STATE)
    if state[0] == 0: # return code good
        state = state[1].split('\n') # split by newline
        if state and len(state) > 1: # more than 2 lines
            state = state[1].split() # second line has tracklist, split by whitespace
            if state and len(state) > 1: #must have more than 1 item
                state = state[1].split('/')
                if state and len(state) > 1: # Current item and total items
                    state = int(state[1])
                else:
                    state =0
            else:
                state = 0
        else:
            state = 0
    else:
        state = 0
    
    return state
    # r = request_post(URL, data=BODY_TRACK_LIST_LENGTH, headers=HEADER)
    # return int(r['result'])
    
def track_consume():
    return mpc_run(MPC_TRACK_CONSUME)
    # body = BODY_TRACK_CONSUME
    # body['params'] = {'value': True}
    # return request_post(URL, data=body, headers=HEADER)
    
def track_shuffle():
    return mpc_run(MPC_TRACK_SHUFFLE)
    # return request_post(URL, data=BODY_TRACK_SHUFFLE, headers=HEADER)

def track_is_playing():
    return '[playing]' in mpc_run(MPC_STATE)[1]
    # r = request_post(URL, data=BODY_STATE, headers=HEADER)
    # return r['result'] == "playing"

def get_volume():
    r = mpc_run(MPC_STATE)[1]
    if r:
        r = r.split()
        if r:
            r = r[1]
        else:
            return 0
    else:
        return 0

    return r

def set_volume(amt):
    return mpc_run(MPC_VOLUME_SET, [amt])

def set_consume_on():
    return mpc_run(MPC_CONSUME_ON)

# Process args
if (sys.argv[1] == 'play' and len(sys.argv) > 2):
    if (sys.argv[2] == 'toggle'):
        print("TOGOGLE PLAY-PAUSE")
        toggle_play_pause()
    else:
        if (track_is_playing() or track_list_length() == 0):
            print("PLAY NOW (replace)")
            play_now(sys.argv[2])
        else:
            print("TRACKLIST ADD")
            track_add(sys.argv[2])

elif(sys.argv[1] == 'playnow'):
    print("PLAY NOW (replace)")
    play_now(sys.argv[2])
elif(sys.argv[1] == 'playnowifempty'):
    print("PLAY NOW (if queue empty)")
    play_now_if_empty(sys.argv[2])
elif (sys.argv[1] == 'pause'):
    print("PAUSE NOW")
    pause()
elif (sys.argv[1] == 'add'):
    print("ADD TRACK")
    track_add(sys.argv[2])
elif (sys.argv[1] == 'next'):
    print("NEXT TRACK")
    track_next()
elif (sys.argv[1] == 'prev'):
    print("PREV TRACK")
    track_prev()
elif (sys.argv[1] == 'clear'):
    print("CLEAR QUEUE")
    track_clear()
elif (sys.argv[1] == 'shuffle'):
    print("SHUFFLE QUEUE")
    track_shuffle()
elif (sys.argv[1] == 'playing'):
    print("Is Playing?")
    print(track_is_playing())
elif (sys.argv[1] == 'volume'):
    if (len(sys.argv) > 2 and sys.argv[2]):
        print("SET VOLUME " + sys.argv[2])
        set_volume(sys.argv[2])
    else:
        print("VOLUME IS " + get_volume())
elif (sys.argv[1] == 'consume'):
    print("SET CONSUME ON")
    set_consume_on()