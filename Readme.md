Summary:
This is two systemd services.
- Mopidy (mopidy.service)
- magic-cards (rfidmusic.service).

Mopidy connects the system audio output with spotify or other music services. It's a music streaming server.

Magic-cards runs a couple node apps (scanner and server). Scanner reads input from the USB RFID reader, and Server hosts a UI which can update the available cards.

rfidmusic.service runs a docker container on startup.
The docker container (magic-cards) is an image with everything related to magic cards configured, and a volume pointing to the local music and magic cards configs (cards.json and custom scripts to interact with Mopidy)

Info:
1. magic-cards docker image
2. Uses Mopidy to access spotify
3. Has hard coded credentials to my spotify account
4. Run the Update script on the Pi to get the latest of everything
5. Run the Install script on the Pi to setup the whole environment


Todo:
X 1. Pick 50 songs from Songs.html and put them into songs.csv
X 2. Script automation to get Spotify URI/Song Art URL for each songs
X 3. Get song art into template for card sticker
X 4. Test print black/white card stickers
X 5. Find way to order card stickers online
X 6. Automate creation of cards.json, move into project directory on Pi
X 7. Apply stickers to 50 cards
X 8. Build enclosure for RFID reader. Wood, plastic?
9. Add a screen to show IP
10. Add a QR code to open UI on phone
X 11. Get audio board working
12. Try a youtube link on a card
13. New enclosure
14. Mopidy.py uses system IP instead of hard coded
15. pull this code from my repo on startup?
16. Get updates to cards.json from the UI synced with git
17. Replace with this: https://github.com/MiczFlor/RPi-Jukebox-RFID

Credentials:
rfidmusic:<shortone>

# Docker logs realtime
<!-- docker logs --timestamps --follow magic-cards -->
# Bash into docker container
<!-- docker exec -it magic-cards /bin/bash -->
# magic-cards services
sudo journalctl -u magiccards-server.service -f
sudo journalctl -u magiccards-scanner.service -f
# mopidy systemd service logs
sudo journalctl -u mopidy.service -f
# rfidmusic systemd service logs
<!-- sudo journalctl -u rfidmusic.service -f -->
# Debug ir-keytable
sudo ir-keytable -t
# Triggerhappy logs
sudo journalctl -u triggerhappy.service -f